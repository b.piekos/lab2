import scala.annotation.tailrec
object Main extends App {

  def jestPierwsza(n: Int): Boolean = {

    @tailrec
    def helper(n: Int, i: Int = 2): Boolean = {
      if (n <= 2)
        if (n == 2)
          return true
        else
          return false
      else if (n % i == 0)
        return false;
      else if (i * i > n)
        return true;
      else helper(n, i + 1)
    }

    helper(n, 2)
  }

  val result = jestPierwsza(11)
  println("Wynik zadania 1:" + result)

  println("----------------------------------------------------")

  def ciąg(n: Int): BigInt = {

    @scala.annotation.tailrec
    def helper(n: Int, acc1: BigInt, acc2: BigInt): BigInt =
      n match {
        case 0 => acc1
        case 1 => acc2
        case _ => helper(n - 1, acc2, acc1 + acc2)
      }

    helper(n, 0, 1)
  }

  val result2 = ciąg(9)
  println("Wynik zadania 2: " + result2)

  println("----------------------------------------------------")

  def uporządkowana(tab: Array[Int], mlr: (Int, Int) => Boolean): Boolean = {
    
    @scala.annotation.tailrec
    def helper(tab: Array[Int], index: Int, )

  }

  var liczby = Array(1, 2, 3, 4)
  def mlr(a: Int, b: Int): Boolean = {
    if (a < b) return true else return false
  }
  val result3 = uporządkowana()

}
